/**
 */
package views;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.AssociationElement#getAssociation <em>Association</em>}</li>
 *   <li>{@link views.AssociationElement#getLinks <em>Links</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getAssociationElement()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='isAssociationOfClass'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot isAssociationOfClass='\n\t\tlet view : View = self.oclContainer().oclContainer().oclAsType(View) in\n\t\t\tself.association -> isEmpty() or (self.association.sourceEnd -> notEmpty() and self.association.sourceEnd.type -> notEmpty() and view.class.effectiveTypes -> includes(self.association.sourceEnd.type))'"
 * @generated
 */
public interface AssociationElement extends ViewElement {
	/**
	 * Returns the value of the '<em><b>Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association</em>' reference.
	 * @see #setAssociation(Association)
	 * @see views.ViewsPackage#getAssociationElement_Association()
	 * @model required="true"
	 * @generated
	 */
	Association getAssociation();

	/**
	 * Sets the value of the '{@link views.AssociationElement#getAssociation <em>Association</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Association</em>' reference.
	 * @see #getAssociation()
	 * @generated
	 */
	void setAssociation(Association value);

	/**
	 * Returns the value of the '<em><b>Links</b></em>' containment reference list.
	 * The list contents are of type {@link views.Link}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Links</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Links</em>' containment reference list.
	 * @see views.ViewsPackage#getAssociationElement_Links()
	 * @model containment="true"
	 * @generated
	 */
	EList<Link> getLinks();

} // AssociationElement
