/**
 */
package views;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>View Element Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.ViewElementGroup#getHeader <em>Header</em>}</li>
 *   <li>{@link views.ViewElementGroup#getViewelements <em>Viewelements</em>}</li>
 *   <li>{@link views.ViewElementGroup#getCondition <em>Condition</em>}</li>
 *   <li>{@link views.ViewElementGroup#getLayout <em>Layout</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getViewElementGroup()
 * @model
 * @generated
 */
public interface ViewElementGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Header</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header</em>' attribute.
	 * @see #setHeader(String)
	 * @see views.ViewsPackage#getViewElementGroup_Header()
	 * @model required="true"
	 * @generated
	 */
	String getHeader();

	/**
	 * Sets the value of the '{@link views.ViewElementGroup#getHeader <em>Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header</em>' attribute.
	 * @see #getHeader()
	 * @generated
	 */
	void setHeader(String value);

	/**
	 * Returns the value of the '<em><b>Viewelements</b></em>' containment reference list.
	 * The list contents are of type {@link views.ViewElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewelements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewelements</em>' containment reference list.
	 * @see views.ViewsPackage#getViewElementGroup_Viewelements()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ViewElement> getViewelements();

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(Condition)
	 * @see views.ViewsPackage#getViewElementGroup_Condition()
	 * @model containment="true"
	 * @generated
	 */
	Condition getCondition();

	/**
	 * Sets the value of the '{@link views.ViewElementGroup#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(Condition value);

	/**
	 * Returns the value of the '<em><b>Layout</b></em>' attribute.
	 * The default value is <code>"Vertical"</code>.
	 * The literals are from the enumeration {@link views.ViewLayout}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout</em>' attribute.
	 * @see views.ViewLayout
	 * @see #setLayout(ViewLayout)
	 * @see views.ViewsPackage#getViewElementGroup_Layout()
	 * @model default="Vertical"
	 * @generated
	 */
	ViewLayout getLayout();

	/**
	 * Sets the value of the '{@link views.ViewElementGroup#getLayout <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout</em>' attribute.
	 * @see views.ViewLayout
	 * @see #getLayout()
	 * @generated
	 */
	void setLayout(ViewLayout value);

} // ViewElementGroup
