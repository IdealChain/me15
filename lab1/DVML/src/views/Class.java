/**
 */
package views;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.Class#getId <em>Id</em>}</li>
 *   <li>{@link views.Class#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link views.Class#getProperties <em>Properties</em>}</li>
 *   <li>{@link views.Class#getEffectiveTypes <em>Effective Types</em>}</li>
 *   <li>{@link views.Class#getEffectiveProperties <em>Effective Properties</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getClass_()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='identifierIsProperty superTypeNotSelf superTypeNotCyclic'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot identifierIsProperty='self.effectiveProperties -> notEmpty() and self.effectiveProperties -> includes(self.id)' superTypeNotSelf='self.superType -> isEmpty() or self.superType <> self' superTypeNotCyclic='self.superType -> isEmpty() or self.superType -> closure(superType) -> excludes(self)'"
 * @generated
 */
public interface Class extends DomainModelType {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' reference.
	 * @see #setId(Property)
	 * @see views.ViewsPackage#getClass_Id()
	 * @model required="true"
	 * @generated
	 */
	Property getId();

	/**
	 * Sets the value of the '{@link views.Class#getId <em>Id</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' reference.
	 * @see #getId()
	 * @generated
	 */
	void setId(Property value);

	/**
	 * Returns the value of the '<em><b>Super Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Type</em>' reference.
	 * @see #setSuperType(Class)
	 * @see views.ViewsPackage#getClass_SuperType()
	 * @model
	 * @generated
	 */
	Class getSuperType();

	/**
	 * Sets the value of the '{@link views.Class#getSuperType <em>Super Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Type</em>' reference.
	 * @see #getSuperType()
	 * @generated
	 */
	void setSuperType(Class value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link views.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see views.ViewsPackage#getClass_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getProperties();

	/**
	 * Returns the value of the '<em><b>Effective Types</b></em>' reference list.
	 * The list contents are of type {@link views.Class}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Effective Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effective Types</em>' reference list.
	 * @see views.ViewsPackage#getClass_EffectiveTypes()
	 * @model changeable="false" volatile="true" derived="true" ordered="false"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='self -> closure(superType)'"
	 * @generated
	 */
	EList<Class> getEffectiveTypes();

	/**
	 * Returns the value of the '<em><b>Effective Properties</b></em>' reference list.
	 * The list contents are of type {@link views.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Effective Properties</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effective Properties</em>' reference list.
	 * @see views.ViewsPackage#getClass_EffectiveProperties()
	 * @model changeable="false" volatile="true" derived="true" ordered="false"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='self -> closure(superType) -> collect(c | c.properties) -> asSet()'"
	 * @generated
	 */
	EList<Property> getEffectiveProperties();

} // Class
