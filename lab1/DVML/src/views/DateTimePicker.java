/**
 */
package views;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Date Time Picker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.DateTimePicker#getFormat <em>Format</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getDateTimePicker()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='dateType'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot dateType='self.property -> isEmpty() or self.property.type -> isEmpty() or (self.property.type.oclIsTypeOf(DataType) and (self.property.type.name = \'Date\' or self.property.type.name = \'Time\' or self.property.type.name = \'DateTime\'))'"
 * @generated
 */
public interface DateTimePicker extends PropertyElement {
	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see #setFormat(String)
	 * @see views.ViewsPackage#getDateTimePicker_Format()
	 * @model
	 * @generated
	 */
	String getFormat();

	/**
	 * Sets the value of the '{@link views.DateTimePicker#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(String value);

} // DateTimePicker
