/**
 */
package views;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comparison Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.ComparisonCondition#getComparisonValue <em>Comparison Value</em>}</li>
 *   <li>{@link views.ComparisonCondition#getComparisonType <em>Comparison Type</em>}</li>
 *   <li>{@link views.ComparisonCondition#getPropertyelement <em>Propertyelement</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getComparisonCondition()
 * @model
 * @generated
 */
public interface ComparisonCondition extends Condition {
	/**
	 * Returns the value of the '<em><b>Comparison Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparison Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparison Value</em>' attribute.
	 * @see #setComparisonValue(Object)
	 * @see views.ViewsPackage#getComparisonCondition_ComparisonValue()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.AnySimpleType" required="true"
	 * @generated
	 */
	Object getComparisonValue();

	/**
	 * Sets the value of the '{@link views.ComparisonCondition#getComparisonValue <em>Comparison Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comparison Value</em>' attribute.
	 * @see #getComparisonValue()
	 * @generated
	 */
	void setComparisonValue(Object value);

	/**
	 * Returns the value of the '<em><b>Comparison Type</b></em>' attribute.
	 * The literals are from the enumeration {@link views.ComparisonType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparison Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparison Type</em>' attribute.
	 * @see views.ComparisonType
	 * @see #setComparisonType(ComparisonType)
	 * @see views.ViewsPackage#getComparisonCondition_ComparisonType()
	 * @model required="true"
	 * @generated
	 */
	ComparisonType getComparisonType();

	/**
	 * Sets the value of the '{@link views.ComparisonCondition#getComparisonType <em>Comparison Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comparison Type</em>' attribute.
	 * @see views.ComparisonType
	 * @see #getComparisonType()
	 * @generated
	 */
	void setComparisonType(ComparisonType value);

	/**
	 * Returns the value of the '<em><b>Propertyelement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Propertyelement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Propertyelement</em>' reference.
	 * @see #setPropertyelement(PropertyElement)
	 * @see views.ViewsPackage#getComparisonCondition_Propertyelement()
	 * @model required="true"
	 * @generated
	 */
	PropertyElement getPropertyelement();

	/**
	 * Sets the value of the '{@link views.ComparisonCondition#getPropertyelement <em>Propertyelement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Propertyelement</em>' reference.
	 * @see #getPropertyelement()
	 * @generated
	 */
	void setPropertyelement(PropertyElement value);

} // ComparisonCondition
