/**
 */
package views;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.Condition#getConditionID <em>Condition ID</em>}</li>
 *   <li>{@link views.Condition#getConditionType <em>Condition Type</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getCondition()
 * @model abstract="true"
 * @generated
 */
public interface Condition extends EObject {
	/**
	 * Returns the value of the '<em><b>Condition ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition ID</em>' attribute.
	 * @see #setConditionID(String)
	 * @see views.ViewsPackage#getCondition_ConditionID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 * @generated
	 */
	String getConditionID();

	/**
	 * Sets the value of the '{@link views.Condition#getConditionID <em>Condition ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition ID</em>' attribute.
	 * @see #getConditionID()
	 * @generated
	 */
	void setConditionID(String value);

	/**
	 * Returns the value of the '<em><b>Condition Type</b></em>' attribute.
	 * The default value is <code>"Hidden"</code>.
	 * The literals are from the enumeration {@link views.ConditionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Type</em>' attribute.
	 * @see views.ConditionType
	 * @see #setConditionType(ConditionType)
	 * @see views.ViewsPackage#getCondition_ConditionType()
	 * @model default="Hidden" required="true"
	 * @generated
	 */
	ConditionType getConditionType();

	/**
	 * Sets the value of the '{@link views.Condition#getConditionType <em>Condition Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Type</em>' attribute.
	 * @see views.ConditionType
	 * @see #getConditionType()
	 * @generated
	 */
	void setConditionType(ConditionType value);

} // Condition
