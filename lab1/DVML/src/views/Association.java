/**
 */
package views;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.Association#getName <em>Name</em>}</li>
 *   <li>{@link views.Association#isIsComposition <em>Is Composition</em>}</li>
 *   <li>{@link views.Association#getNavigableEnd <em>Navigable End</em>}</li>
 *   <li>{@link views.Association#getSourceEnd <em>Source End</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getAssociation()
 * @model
 * @generated
 */
public interface Association extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see views.ViewsPackage#getAssociation_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link views.Association#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Is Composition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Composition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Composition</em>' attribute.
	 * @see #setIsComposition(boolean)
	 * @see views.ViewsPackage#getAssociation_IsComposition()
	 * @model required="true"
	 * @generated
	 */
	boolean isIsComposition();

	/**
	 * Sets the value of the '{@link views.Association#isIsComposition <em>Is Composition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Composition</em>' attribute.
	 * @see #isIsComposition()
	 * @generated
	 */
	void setIsComposition(boolean value);

	/**
	 * Returns the value of the '<em><b>Navigable End</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Navigable End</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Navigable End</em>' containment reference.
	 * @see #setNavigableEnd(Property)
	 * @see views.ViewsPackage#getAssociation_NavigableEnd()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Property getNavigableEnd();

	/**
	 * Sets the value of the '{@link views.Association#getNavigableEnd <em>Navigable End</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Navigable End</em>' containment reference.
	 * @see #getNavigableEnd()
	 * @generated
	 */
	void setNavigableEnd(Property value);

	/**
	 * Returns the value of the '<em><b>Source End</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source End</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source End</em>' containment reference.
	 * @see #setSourceEnd(Property)
	 * @see views.ViewsPackage#getAssociation_SourceEnd()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Property getSourceEnd();

	/**
	 * Sets the value of the '{@link views.Association#getSourceEnd <em>Source End</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source End</em>' containment reference.
	 * @see #getSourceEnd()
	 * @generated
	 */
	void setSourceEnd(Property value);

} // Association
