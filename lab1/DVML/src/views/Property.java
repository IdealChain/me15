/**
 */
package views;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.Property#getName <em>Name</em>}</li>
 *   <li>{@link views.Property#getType <em>Type</em>}</li>
 *   <li>{@link views.Property#getMultiplicity <em>Multiplicity</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getProperty()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='classPropertyType classPropertyMultiplicity classPropertyIdMandatory associationPropertyType associationPropertyMultiplicity'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot classPropertyType='not self.oclContainer().oclIsKindOf(Class) or self.type.oclIsTypeOf(Enumeration) or self.type.oclIsTypeOf(DataType)' classPropertyMultiplicity='not self.oclContainer().oclIsKindOf(Class) or (multiplicity -> notEmpty() and multiplicity.lowerBound >= 0 and multiplicity.lowerBound <= 1 and multiplicity.upperBound = 1)' classPropertyIdMandatory='not self.oclContainer().oclIsKindOf(Class) or Class.allInstances() -> collect(c | c.id) -> excludes(self) or (multiplicity -> notEmpty() and multiplicity.lowerBound = 1)' associationPropertyType='not self.oclContainer().oclIsKindOf(Association) or self.type.oclIsTypeOf(Class)' associationPropertyMultiplicity='not self.oclContainer().oclIsKindOf(Association) or (multiplicity -> notEmpty() and multiplicity.lowerBound >= 0 and (multiplicity.upperBound = -1 or (multiplicity.upperBound > 0 and multiplicity.upperBound >= multiplicity.lowerBound)))'"
 * @generated
 */
public interface Property extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see views.ViewsPackage#getProperty_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link views.Property#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(DomainModelType)
	 * @see views.ViewsPackage#getProperty_Type()
	 * @model required="true"
	 * @generated
	 */
	DomainModelType getType();

	/**
	 * Sets the value of the '{@link views.Property#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(DomainModelType value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see views.ViewsPackage#getProperty_Multiplicity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link views.Property#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

} // Property
