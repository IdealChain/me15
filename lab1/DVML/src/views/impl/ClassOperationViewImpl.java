/**
 */
package views.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import views.ClassOperationType;
import views.ClassOperationView;
import views.ViewElementGroup;
import views.ViewLayout;
import views.ViewsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Operation View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link views.impl.ClassOperationViewImpl#getViewelementgroups <em>Viewelementgroups</em>}</li>
 *   <li>{@link views.impl.ClassOperationViewImpl#getOperationType <em>Operation Type</em>}</li>
 *   <li>{@link views.impl.ClassOperationViewImpl#getLayout <em>Layout</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassOperationViewImpl extends ViewImpl implements ClassOperationView {
	/**
	 * The cached value of the '{@link #getViewelementgroups() <em>Viewelementgroups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getViewelementgroups()
	 * @generated
	 * @ordered
	 */
	protected EList<ViewElementGroup> viewelementgroups;

	/**
	 * The default value of the '{@link #getOperationType() <em>Operation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationType()
	 * @generated
	 * @ordered
	 */
	protected static final ClassOperationType OPERATION_TYPE_EDEFAULT = ClassOperationType.READ;

	/**
	 * The cached value of the '{@link #getOperationType() <em>Operation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationType()
	 * @generated
	 * @ordered
	 */
	protected ClassOperationType operationType = OPERATION_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLayout() <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayout()
	 * @generated
	 * @ordered
	 */
	protected static final ViewLayout LAYOUT_EDEFAULT = ViewLayout.VERTICAL;

	/**
	 * The cached value of the '{@link #getLayout() <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayout()
	 * @generated
	 * @ordered
	 */
	protected ViewLayout layout = LAYOUT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassOperationViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ViewsPackage.Literals.CLASS_OPERATION_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ViewElementGroup> getViewelementgroups() {
		if (viewelementgroups == null) {
			viewelementgroups = new EObjectContainmentEList<ViewElementGroup>(ViewElementGroup.class, this, ViewsPackage.CLASS_OPERATION_VIEW__VIEWELEMENTGROUPS);
		}
		return viewelementgroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassOperationType getOperationType() {
		return operationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationType(ClassOperationType newOperationType) {
		ClassOperationType oldOperationType = operationType;
		operationType = newOperationType == null ? OPERATION_TYPE_EDEFAULT : newOperationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ViewsPackage.CLASS_OPERATION_VIEW__OPERATION_TYPE, oldOperationType, operationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewLayout getLayout() {
		return layout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLayout(ViewLayout newLayout) {
		ViewLayout oldLayout = layout;
		layout = newLayout == null ? LAYOUT_EDEFAULT : newLayout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ViewsPackage.CLASS_OPERATION_VIEW__LAYOUT, oldLayout, layout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ViewsPackage.CLASS_OPERATION_VIEW__VIEWELEMENTGROUPS:
				return ((InternalEList<?>)getViewelementgroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ViewsPackage.CLASS_OPERATION_VIEW__VIEWELEMENTGROUPS:
				return getViewelementgroups();
			case ViewsPackage.CLASS_OPERATION_VIEW__OPERATION_TYPE:
				return getOperationType();
			case ViewsPackage.CLASS_OPERATION_VIEW__LAYOUT:
				return getLayout();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ViewsPackage.CLASS_OPERATION_VIEW__VIEWELEMENTGROUPS:
				getViewelementgroups().clear();
				getViewelementgroups().addAll((Collection<? extends ViewElementGroup>)newValue);
				return;
			case ViewsPackage.CLASS_OPERATION_VIEW__OPERATION_TYPE:
				setOperationType((ClassOperationType)newValue);
				return;
			case ViewsPackage.CLASS_OPERATION_VIEW__LAYOUT:
				setLayout((ViewLayout)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ViewsPackage.CLASS_OPERATION_VIEW__VIEWELEMENTGROUPS:
				getViewelementgroups().clear();
				return;
			case ViewsPackage.CLASS_OPERATION_VIEW__OPERATION_TYPE:
				setOperationType(OPERATION_TYPE_EDEFAULT);
				return;
			case ViewsPackage.CLASS_OPERATION_VIEW__LAYOUT:
				setLayout(LAYOUT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ViewsPackage.CLASS_OPERATION_VIEW__VIEWELEMENTGROUPS:
				return viewelementgroups != null && !viewelementgroups.isEmpty();
			case ViewsPackage.CLASS_OPERATION_VIEW__OPERATION_TYPE:
				return operationType != OPERATION_TYPE_EDEFAULT;
			case ViewsPackage.CLASS_OPERATION_VIEW__LAYOUT:
				return layout != LAYOUT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operationType: ");
		result.append(operationType);
		result.append(", layout: ");
		result.append(layout);
		result.append(')');
		return result.toString();
	}

} //ClassOperationViewImpl
