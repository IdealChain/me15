/**
 */
package views.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import views.CompositeCondition;
import views.CompositeConditionType;
import views.Condition;
import views.ViewsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link views.impl.CompositeConditionImpl#getCompositeConditionType <em>Composite Condition Type</em>}</li>
 *   <li>{@link views.impl.CompositeConditionImpl#getConditions <em>Conditions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeConditionImpl extends ConditionImpl implements CompositeCondition {
	/**
	 * The default value of the '{@link #getCompositeConditionType() <em>Composite Condition Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompositeConditionType()
	 * @generated
	 * @ordered
	 */
	protected static final CompositeConditionType COMPOSITE_CONDITION_TYPE_EDEFAULT = CompositeConditionType.AND;

	/**
	 * The cached value of the '{@link #getCompositeConditionType() <em>Composite Condition Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompositeConditionType()
	 * @generated
	 * @ordered
	 */
	protected CompositeConditionType compositeConditionType = COMPOSITE_CONDITION_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConditions() <em>Conditions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<Condition> conditions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ViewsPackage.Literals.COMPOSITE_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeConditionType getCompositeConditionType() {
		return compositeConditionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompositeConditionType(CompositeConditionType newCompositeConditionType) {
		CompositeConditionType oldCompositeConditionType = compositeConditionType;
		compositeConditionType = newCompositeConditionType == null ? COMPOSITE_CONDITION_TYPE_EDEFAULT : newCompositeConditionType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ViewsPackage.COMPOSITE_CONDITION__COMPOSITE_CONDITION_TYPE, oldCompositeConditionType, compositeConditionType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Condition> getConditions() {
		if (conditions == null) {
			conditions = new EObjectContainmentEList<Condition>(Condition.class, this, ViewsPackage.COMPOSITE_CONDITION__CONDITIONS);
		}
		return conditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ViewsPackage.COMPOSITE_CONDITION__CONDITIONS:
				return ((InternalEList<?>)getConditions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ViewsPackage.COMPOSITE_CONDITION__COMPOSITE_CONDITION_TYPE:
				return getCompositeConditionType();
			case ViewsPackage.COMPOSITE_CONDITION__CONDITIONS:
				return getConditions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ViewsPackage.COMPOSITE_CONDITION__COMPOSITE_CONDITION_TYPE:
				setCompositeConditionType((CompositeConditionType)newValue);
				return;
			case ViewsPackage.COMPOSITE_CONDITION__CONDITIONS:
				getConditions().clear();
				getConditions().addAll((Collection<? extends Condition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ViewsPackage.COMPOSITE_CONDITION__COMPOSITE_CONDITION_TYPE:
				setCompositeConditionType(COMPOSITE_CONDITION_TYPE_EDEFAULT);
				return;
			case ViewsPackage.COMPOSITE_CONDITION__CONDITIONS:
				getConditions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ViewsPackage.COMPOSITE_CONDITION__COMPOSITE_CONDITION_TYPE:
				return compositeConditionType != COMPOSITE_CONDITION_TYPE_EDEFAULT;
			case ViewsPackage.COMPOSITE_CONDITION__CONDITIONS:
				return conditions != null && !conditions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (compositeConditionType: ");
		result.append(compositeConditionType);
		result.append(')');
		return result.toString();
	}

} //CompositeConditionImpl
