/**
 */
package views.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import views.Association;
import views.ClassIndexView;
import views.ClassOperationType;
import views.ClassOperationView;
import views.ComparisonCondition;
import views.ComparisonType;
import views.CompositeCondition;
import views.CompositeConditionType;
import views.ConditionType;
import views.DataType;
import views.DateTimePicker;
import views.DomainModel;
import views.Enumeration;
import views.EnumerationLiteral;
import views.EnumerationLiteralItem;
import views.Link;
import views.List;
import views.Multiplicity;
import views.Property;
import views.Selection;
import views.SelectionItem;
import views.Table;
import views.Text;
import views.ViewElementGroup;
import views.ViewGroup;
import views.ViewLayout;
import views.ViewModel;
import views.ViewsFactory;
import views.ViewsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ViewsFactoryImpl extends EFactoryImpl implements ViewsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ViewsFactory init() {
		try {
			ViewsFactory theViewsFactory = (ViewsFactory)EPackage.Registry.INSTANCE.getEFactory(ViewsPackage.eNS_URI);
			if (theViewsFactory != null) {
				return theViewsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ViewsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ViewsPackage.DOMAIN_MODEL: return createDomainModel();
			case ViewsPackage.CLASS: return createClass();
			case ViewsPackage.PROPERTY: return createProperty();
			case ViewsPackage.DATA_TYPE: return createDataType();
			case ViewsPackage.ASSOCIATION: return createAssociation();
			case ViewsPackage.ENUMERATION: return createEnumeration();
			case ViewsPackage.ENUMERATION_LITERAL: return createEnumerationLiteral();
			case ViewsPackage.VIEW_MODEL: return createViewModel();
			case ViewsPackage.VIEW_GROUP: return createViewGroup();
			case ViewsPackage.CLASS_INDEX_VIEW: return createClassIndexView();
			case ViewsPackage.CLASS_OPERATION_VIEW: return createClassOperationView();
			case ViewsPackage.VIEW_ELEMENT_GROUP: return createViewElementGroup();
			case ViewsPackage.LIST: return createList();
			case ViewsPackage.TABLE: return createTable();
			case ViewsPackage.TEXT: return createText();
			case ViewsPackage.DATE_TIME_PICKER: return createDateTimePicker();
			case ViewsPackage.SELECTION: return createSelection();
			case ViewsPackage.SELECTION_ITEM: return createSelectionItem();
			case ViewsPackage.ENUMERATION_LITERAL_ITEM: return createEnumerationLiteralItem();
			case ViewsPackage.COMPARISON_CONDITION: return createComparisonCondition();
			case ViewsPackage.COMPOSITE_CONDITION: return createCompositeCondition();
			case ViewsPackage.LINK: return createLink();
			case ViewsPackage.MULTIPLICITY: return createMultiplicity();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ViewsPackage.CONDITION_TYPE:
				return createConditionTypeFromString(eDataType, initialValue);
			case ViewsPackage.COMPARISON_TYPE:
				return createComparisonTypeFromString(eDataType, initialValue);
			case ViewsPackage.COMPOSITE_CONDITION_TYPE:
				return createCompositeConditionTypeFromString(eDataType, initialValue);
			case ViewsPackage.VIEW_LAYOUT:
				return createViewLayoutFromString(eDataType, initialValue);
			case ViewsPackage.CLASS_OPERATION_TYPE:
				return createClassOperationTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ViewsPackage.CONDITION_TYPE:
				return convertConditionTypeToString(eDataType, instanceValue);
			case ViewsPackage.COMPARISON_TYPE:
				return convertComparisonTypeToString(eDataType, instanceValue);
			case ViewsPackage.COMPOSITE_CONDITION_TYPE:
				return convertCompositeConditionTypeToString(eDataType, instanceValue);
			case ViewsPackage.VIEW_LAYOUT:
				return convertViewLayoutToString(eDataType, instanceValue);
			case ViewsPackage.CLASS_OPERATION_TYPE:
				return convertClassOperationTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainModel createDomainModel() {
		DomainModelImpl domainModel = new DomainModelImpl();
		return domainModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public views.Class createClass() {
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType createDataType() {
		DataTypeImpl dataType = new DataTypeImpl();
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association createAssociation() {
		AssociationImpl association = new AssociationImpl();
		return association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration createEnumeration() {
		EnumerationImpl enumeration = new EnumerationImpl();
		return enumeration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral createEnumerationLiteral() {
		EnumerationLiteralImpl enumerationLiteral = new EnumerationLiteralImpl();
		return enumerationLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewModel createViewModel() {
		ViewModelImpl viewModel = new ViewModelImpl();
		return viewModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewGroup createViewGroup() {
		ViewGroupImpl viewGroup = new ViewGroupImpl();
		return viewGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassIndexView createClassIndexView() {
		ClassIndexViewImpl classIndexView = new ClassIndexViewImpl();
		return classIndexView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassOperationView createClassOperationView() {
		ClassOperationViewImpl classOperationView = new ClassOperationViewImpl();
		return classOperationView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewElementGroup createViewElementGroup() {
		ViewElementGroupImpl viewElementGroup = new ViewElementGroupImpl();
		return viewElementGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List createList() {
		ListImpl list = new ListImpl();
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Table createTable() {
		TableImpl table = new TableImpl();
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Text createText() {
		TextImpl text = new TextImpl();
		return text;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DateTimePicker createDateTimePicker() {
		DateTimePickerImpl dateTimePicker = new DateTimePickerImpl();
		return dateTimePicker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Selection createSelection() {
		SelectionImpl selection = new SelectionImpl();
		return selection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectionItem createSelectionItem() {
		SelectionItemImpl selectionItem = new SelectionItemImpl();
		return selectionItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteralItem createEnumerationLiteralItem() {
		EnumerationLiteralItemImpl enumerationLiteralItem = new EnumerationLiteralItemImpl();
		return enumerationLiteralItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonCondition createComparisonCondition() {
		ComparisonConditionImpl comparisonCondition = new ComparisonConditionImpl();
		return comparisonCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeCondition createCompositeCondition() {
		CompositeConditionImpl compositeCondition = new CompositeConditionImpl();
		return compositeCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Link createLink() {
		LinkImpl link = new LinkImpl();
		return link;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity createMultiplicity() {
		MultiplicityImpl multiplicity = new MultiplicityImpl();
		return multiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionType createConditionTypeFromString(EDataType eDataType, String initialValue) {
		ConditionType result = ConditionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConditionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonType createComparisonTypeFromString(EDataType eDataType, String initialValue) {
		ComparisonType result = ComparisonType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertComparisonTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeConditionType createCompositeConditionTypeFromString(EDataType eDataType, String initialValue) {
		CompositeConditionType result = CompositeConditionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCompositeConditionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewLayout createViewLayoutFromString(EDataType eDataType, String initialValue) {
		ViewLayout result = ViewLayout.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertViewLayoutToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassOperationType createClassOperationTypeFromString(EDataType eDataType, String initialValue) {
		ClassOperationType result = ClassOperationType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertClassOperationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewsPackage getViewsPackage() {
		return (ViewsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ViewsPackage getPackage() {
		return ViewsPackage.eINSTANCE;
	}

} //ViewsFactoryImpl
