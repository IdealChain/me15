/**
 */
package views.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import views.ComparisonCondition;
import views.ComparisonType;
import views.PropertyElement;
import views.ViewsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comparison Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link views.impl.ComparisonConditionImpl#getComparisonValue <em>Comparison Value</em>}</li>
 *   <li>{@link views.impl.ComparisonConditionImpl#getComparisonType <em>Comparison Type</em>}</li>
 *   <li>{@link views.impl.ComparisonConditionImpl#getPropertyelement <em>Propertyelement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComparisonConditionImpl extends ConditionImpl implements ComparisonCondition {
	/**
	 * The default value of the '{@link #getComparisonValue() <em>Comparison Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComparisonValue()
	 * @generated
	 * @ordered
	 */
	protected static final Object COMPARISON_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getComparisonValue() <em>Comparison Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComparisonValue()
	 * @generated
	 * @ordered
	 */
	protected Object comparisonValue = COMPARISON_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getComparisonType() <em>Comparison Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComparisonType()
	 * @generated
	 * @ordered
	 */
	protected static final ComparisonType COMPARISON_TYPE_EDEFAULT = ComparisonType.EQUAL_TO;

	/**
	 * The cached value of the '{@link #getComparisonType() <em>Comparison Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComparisonType()
	 * @generated
	 * @ordered
	 */
	protected ComparisonType comparisonType = COMPARISON_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPropertyelement() <em>Propertyelement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyelement()
	 * @generated
	 * @ordered
	 */
	protected PropertyElement propertyelement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComparisonConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ViewsPackage.Literals.COMPARISON_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getComparisonValue() {
		return comparisonValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComparisonValue(Object newComparisonValue) {
		Object oldComparisonValue = comparisonValue;
		comparisonValue = newComparisonValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ViewsPackage.COMPARISON_CONDITION__COMPARISON_VALUE, oldComparisonValue, comparisonValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonType getComparisonType() {
		return comparisonType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComparisonType(ComparisonType newComparisonType) {
		ComparisonType oldComparisonType = comparisonType;
		comparisonType = newComparisonType == null ? COMPARISON_TYPE_EDEFAULT : newComparisonType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ViewsPackage.COMPARISON_CONDITION__COMPARISON_TYPE, oldComparisonType, comparisonType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyElement getPropertyelement() {
		if (propertyelement != null && propertyelement.eIsProxy()) {
			InternalEObject oldPropertyelement = (InternalEObject)propertyelement;
			propertyelement = (PropertyElement)eResolveProxy(oldPropertyelement);
			if (propertyelement != oldPropertyelement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ViewsPackage.COMPARISON_CONDITION__PROPERTYELEMENT, oldPropertyelement, propertyelement));
			}
		}
		return propertyelement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyElement basicGetPropertyelement() {
		return propertyelement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyelement(PropertyElement newPropertyelement) {
		PropertyElement oldPropertyelement = propertyelement;
		propertyelement = newPropertyelement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ViewsPackage.COMPARISON_CONDITION__PROPERTYELEMENT, oldPropertyelement, propertyelement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ViewsPackage.COMPARISON_CONDITION__COMPARISON_VALUE:
				return getComparisonValue();
			case ViewsPackage.COMPARISON_CONDITION__COMPARISON_TYPE:
				return getComparisonType();
			case ViewsPackage.COMPARISON_CONDITION__PROPERTYELEMENT:
				if (resolve) return getPropertyelement();
				return basicGetPropertyelement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ViewsPackage.COMPARISON_CONDITION__COMPARISON_VALUE:
				setComparisonValue(newValue);
				return;
			case ViewsPackage.COMPARISON_CONDITION__COMPARISON_TYPE:
				setComparisonType((ComparisonType)newValue);
				return;
			case ViewsPackage.COMPARISON_CONDITION__PROPERTYELEMENT:
				setPropertyelement((PropertyElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ViewsPackage.COMPARISON_CONDITION__COMPARISON_VALUE:
				setComparisonValue(COMPARISON_VALUE_EDEFAULT);
				return;
			case ViewsPackage.COMPARISON_CONDITION__COMPARISON_TYPE:
				setComparisonType(COMPARISON_TYPE_EDEFAULT);
				return;
			case ViewsPackage.COMPARISON_CONDITION__PROPERTYELEMENT:
				setPropertyelement((PropertyElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ViewsPackage.COMPARISON_CONDITION__COMPARISON_VALUE:
				return COMPARISON_VALUE_EDEFAULT == null ? comparisonValue != null : !COMPARISON_VALUE_EDEFAULT.equals(comparisonValue);
			case ViewsPackage.COMPARISON_CONDITION__COMPARISON_TYPE:
				return comparisonType != COMPARISON_TYPE_EDEFAULT;
			case ViewsPackage.COMPARISON_CONDITION__PROPERTYELEMENT:
				return propertyelement != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (comparisonValue: ");
		result.append(comparisonValue);
		result.append(", comparisonType: ");
		result.append(comparisonType);
		result.append(')');
		return result.toString();
	}

} //ComparisonConditionImpl
