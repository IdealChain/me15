/**
 */
package views;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>View Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.ViewModel#getViewgroups <em>Viewgroups</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getViewModel()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validWelcomeViewGroup'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot validWelcomeViewGroup='self.viewgroups -> select(v | v.welcomeViewGroup = true) -> size() = 1'"
 * @generated
 */
public interface ViewModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Viewgroups</b></em>' containment reference list.
	 * The list contents are of type {@link views.ViewGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewgroups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewgroups</em>' containment reference list.
	 * @see views.ViewsPackage#getViewModel_Viewgroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<ViewGroup> getViewgroups();

} // ViewModel
