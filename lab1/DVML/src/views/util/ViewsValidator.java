/**
 */
package views.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import views.Association;
import views.AssociationElement;
import views.ClassIndexView;
import views.ClassOperationType;
import views.ClassOperationView;
import views.ComparisonCondition;
import views.ComparisonType;
import views.CompositeCondition;
import views.CompositeConditionType;
import views.Condition;
import views.ConditionType;
import views.DataType;
import views.DateTimePicker;
import views.DomainModel;
import views.DomainModelType;
import views.Enumeration;
import views.EnumerationLiteral;
import views.EnumerationLiteralItem;
import views.Link;
import views.List;
import views.Multiplicity;
import views.Property;
import views.PropertyElement;
import views.Selection;
import views.SelectionItem;
import views.Table;
import views.Text;
import views.View;
import views.ViewElement;
import views.ViewElementGroup;
import views.ViewGroup;
import views.ViewLayout;
import views.ViewModel;
import views.ViewsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see views.ViewsPackage
 * @generated
 */
public class ViewsValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ViewsValidator INSTANCE = new ViewsValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "views";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewsValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ViewsPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ViewsPackage.DOMAIN_MODEL:
				return validateDomainModel((DomainModel)value, diagnostics, context);
			case ViewsPackage.CLASS:
				return validateClass((views.Class)value, diagnostics, context);
			case ViewsPackage.PROPERTY:
				return validateProperty((Property)value, diagnostics, context);
			case ViewsPackage.DATA_TYPE:
				return validateDataType((DataType)value, diagnostics, context);
			case ViewsPackage.ASSOCIATION:
				return validateAssociation((Association)value, diagnostics, context);
			case ViewsPackage.ENUMERATION:
				return validateEnumeration((Enumeration)value, diagnostics, context);
			case ViewsPackage.ENUMERATION_LITERAL:
				return validateEnumerationLiteral((EnumerationLiteral)value, diagnostics, context);
			case ViewsPackage.VIEW_MODEL:
				return validateViewModel((ViewModel)value, diagnostics, context);
			case ViewsPackage.VIEW_GROUP:
				return validateViewGroup((ViewGroup)value, diagnostics, context);
			case ViewsPackage.VIEW:
				return validateView((View)value, diagnostics, context);
			case ViewsPackage.CLASS_INDEX_VIEW:
				return validateClassIndexView((ClassIndexView)value, diagnostics, context);
			case ViewsPackage.CLASS_OPERATION_VIEW:
				return validateClassOperationView((ClassOperationView)value, diagnostics, context);
			case ViewsPackage.VIEW_ELEMENT:
				return validateViewElement((ViewElement)value, diagnostics, context);
			case ViewsPackage.PROPERTY_ELEMENT:
				return validatePropertyElement((PropertyElement)value, diagnostics, context);
			case ViewsPackage.ASSOCIATION_ELEMENT:
				return validateAssociationElement((AssociationElement)value, diagnostics, context);
			case ViewsPackage.VIEW_ELEMENT_GROUP:
				return validateViewElementGroup((ViewElementGroup)value, diagnostics, context);
			case ViewsPackage.LIST:
				return validateList((List)value, diagnostics, context);
			case ViewsPackage.TABLE:
				return validateTable((Table)value, diagnostics, context);
			case ViewsPackage.TEXT:
				return validateText((Text)value, diagnostics, context);
			case ViewsPackage.DATE_TIME_PICKER:
				return validateDateTimePicker((DateTimePicker)value, diagnostics, context);
			case ViewsPackage.SELECTION:
				return validateSelection((Selection)value, diagnostics, context);
			case ViewsPackage.SELECTION_ITEM:
				return validateSelectionItem((SelectionItem)value, diagnostics, context);
			case ViewsPackage.ENUMERATION_LITERAL_ITEM:
				return validateEnumerationLiteralItem((EnumerationLiteralItem)value, diagnostics, context);
			case ViewsPackage.CONDITION:
				return validateCondition((Condition)value, diagnostics, context);
			case ViewsPackage.COMPARISON_CONDITION:
				return validateComparisonCondition((ComparisonCondition)value, diagnostics, context);
			case ViewsPackage.COMPOSITE_CONDITION:
				return validateCompositeCondition((CompositeCondition)value, diagnostics, context);
			case ViewsPackage.LINK:
				return validateLink((Link)value, diagnostics, context);
			case ViewsPackage.DOMAIN_MODEL_TYPE:
				return validateDomainModelType((DomainModelType)value, diagnostics, context);
			case ViewsPackage.MULTIPLICITY:
				return validateMultiplicity((Multiplicity)value, diagnostics, context);
			case ViewsPackage.CONDITION_TYPE:
				return validateConditionType((ConditionType)value, diagnostics, context);
			case ViewsPackage.COMPARISON_TYPE:
				return validateComparisonType((ComparisonType)value, diagnostics, context);
			case ViewsPackage.COMPOSITE_CONDITION_TYPE:
				return validateCompositeConditionType((CompositeConditionType)value, diagnostics, context);
			case ViewsPackage.VIEW_LAYOUT:
				return validateViewLayout((ViewLayout)value, diagnostics, context);
			case ViewsPackage.CLASS_OPERATION_TYPE:
				return validateClassOperationType((ClassOperationType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDomainModel(DomainModel domainModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(domainModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClass(views.Class class_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(class_, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validateClass_identifierIsProperty(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validateClass_superTypeNotSelf(class_, diagnostics, context);
		if (result || diagnostics != null) result &= validateClass_superTypeNotCyclic(class_, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the identifierIsProperty constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS__IDENTIFIER_IS_PROPERTY__EEXPRESSION = "self.effectiveProperties -> notEmpty() and self.effectiveProperties -> includes(self.id)";

	/**
	 * Validates the identifierIsProperty constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClass_identifierIsProperty(views.Class class_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.CLASS,
				 class_,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "identifierIsProperty",
				 CLASS__IDENTIFIER_IS_PROPERTY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the superTypeNotSelf constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS__SUPER_TYPE_NOT_SELF__EEXPRESSION = "self.superType -> isEmpty() or self.superType <> self";

	/**
	 * Validates the superTypeNotSelf constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClass_superTypeNotSelf(views.Class class_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.CLASS,
				 class_,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "superTypeNotSelf",
				 CLASS__SUPER_TYPE_NOT_SELF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the superTypeNotCyclic constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CLASS__SUPER_TYPE_NOT_CYCLIC__EEXPRESSION = "self.superType -> isEmpty() or self.superType -> closure(superType) -> excludes(self)";

	/**
	 * Validates the superTypeNotCyclic constraint of '<em>Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClass_superTypeNotCyclic(views.Class class_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.CLASS,
				 class_,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "superTypeNotCyclic",
				 CLASS__SUPER_TYPE_NOT_CYCLIC__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProperty(Property property, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(property, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(property, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(property, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(property, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(property, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(property, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(property, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(property, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(property, diagnostics, context);
		if (result || diagnostics != null) result &= validateProperty_classPropertyType(property, diagnostics, context);
		if (result || diagnostics != null) result &= validateProperty_classPropertyMultiplicity(property, diagnostics, context);
		if (result || diagnostics != null) result &= validateProperty_classPropertyIdMandatory(property, diagnostics, context);
		if (result || diagnostics != null) result &= validateProperty_associationPropertyType(property, diagnostics, context);
		if (result || diagnostics != null) result &= validateProperty_associationPropertyMultiplicity(property, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the classPropertyType constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PROPERTY__CLASS_PROPERTY_TYPE__EEXPRESSION = "not self.oclContainer().oclIsKindOf(Class) or self.type.oclIsTypeOf(Enumeration) or self.type.oclIsTypeOf(DataType)";

	/**
	 * Validates the classPropertyType constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProperty_classPropertyType(Property property, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.PROPERTY,
				 property,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "classPropertyType",
				 PROPERTY__CLASS_PROPERTY_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the classPropertyMultiplicity constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PROPERTY__CLASS_PROPERTY_MULTIPLICITY__EEXPRESSION = "not self.oclContainer().oclIsKindOf(Class) or (multiplicity -> notEmpty() and multiplicity.lowerBound >= 0 and multiplicity.lowerBound <= 1 and multiplicity.upperBound = 1)";

	/**
	 * Validates the classPropertyMultiplicity constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProperty_classPropertyMultiplicity(Property property, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.PROPERTY,
				 property,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "classPropertyMultiplicity",
				 PROPERTY__CLASS_PROPERTY_MULTIPLICITY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the classPropertyIdMandatory constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PROPERTY__CLASS_PROPERTY_ID_MANDATORY__EEXPRESSION = "not self.oclContainer().oclIsKindOf(Class) or Class.allInstances() -> collect(c | c.id) -> excludes(self) or (multiplicity -> notEmpty() and multiplicity.lowerBound = 1)";

	/**
	 * Validates the classPropertyIdMandatory constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProperty_classPropertyIdMandatory(Property property, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.PROPERTY,
				 property,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "classPropertyIdMandatory",
				 PROPERTY__CLASS_PROPERTY_ID_MANDATORY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the associationPropertyType constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PROPERTY__ASSOCIATION_PROPERTY_TYPE__EEXPRESSION = "not self.oclContainer().oclIsKindOf(Association) or self.type.oclIsTypeOf(Class)";

	/**
	 * Validates the associationPropertyType constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProperty_associationPropertyType(Property property, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.PROPERTY,
				 property,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "associationPropertyType",
				 PROPERTY__ASSOCIATION_PROPERTY_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the associationPropertyMultiplicity constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PROPERTY__ASSOCIATION_PROPERTY_MULTIPLICITY__EEXPRESSION = "not self.oclContainer().oclIsKindOf(Association) or (multiplicity -> notEmpty() and multiplicity.lowerBound >= 0 and (multiplicity.upperBound = -1 or (multiplicity.upperBound > 0 and multiplicity.upperBound >= multiplicity.lowerBound)))";

	/**
	 * Validates the associationPropertyMultiplicity constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProperty_associationPropertyMultiplicity(Property property, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.PROPERTY,
				 property,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "associationPropertyMultiplicity",
				 PROPERTY__ASSOCIATION_PROPERTY_MULTIPLICITY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataType(DataType dataType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(dataType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociation(Association association, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(association, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumeration(Enumeration enumeration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumeration, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumerationLiteral(EnumerationLiteral enumerationLiteral, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumerationLiteral, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewModel(ViewModel viewModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(viewModel, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(viewModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(viewModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(viewModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(viewModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(viewModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(viewModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(viewModel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(viewModel, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewModel_validWelcomeViewGroup(viewModel, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the validWelcomeViewGroup constraint of '<em>View Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VIEW_MODEL__VALID_WELCOME_VIEW_GROUP__EEXPRESSION = "self.viewgroups -> select(v | v.welcomeViewGroup = true) -> size() = 1";

	/**
	 * Validates the validWelcomeViewGroup constraint of '<em>View Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewModel_validWelcomeViewGroup(ViewModel viewModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.VIEW_MODEL,
				 viewModel,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "validWelcomeViewGroup",
				 VIEW_MODEL__VALID_WELCOME_VIEW_GROUP__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewGroup(ViewGroup viewGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(viewGroup, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(viewGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(viewGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(viewGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(viewGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(viewGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(viewGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(viewGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(viewGroup, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewGroup_validStartView(viewGroup, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the validStartView constraint of '<em>View Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VIEW_GROUP__VALID_START_VIEW__EEXPRESSION = "self.views -> select(v | v.startView = true) -> size() = 1";

	/**
	 * Validates the validStartView constraint of '<em>View Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewGroup_validStartView(ViewGroup viewGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.VIEW_GROUP,
				 viewGroup,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "validStartView",
				 VIEW_GROUP__VALID_START_VIEW__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateView(View view, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(view, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassIndexView(ClassIndexView classIndexView, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(classIndexView, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassOperationView(ClassOperationView classOperationView, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(classOperationView, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewElement(ViewElement viewElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(viewElement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(viewElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(viewElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(viewElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(viewElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(viewElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(viewElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(viewElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(viewElement, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewElement_elementIDUnique(viewElement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the elementIDUnique constraint of '<em>View Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VIEW_ELEMENT__ELEMENT_ID_UNIQUE__EEXPRESSION = "ViewElement.allInstances() -> forAll(v1,v2 | v1 <> v2 implies v1.elementID <> v2.elementID)";

	/**
	 * Validates the elementIDUnique constraint of '<em>View Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewElement_elementIDUnique(ViewElement viewElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.VIEW_ELEMENT,
				 viewElement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "elementIDUnique",
				 VIEW_ELEMENT__ELEMENT_ID_UNIQUE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePropertyElement(PropertyElement propertyElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(propertyElement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewElement_elementIDUnique(propertyElement, diagnostics, context);
		if (result || diagnostics != null) result &= validatePropertyElement_isPropertyOfClass(propertyElement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the isPropertyOfClass constraint of '<em>Property Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PROPERTY_ELEMENT__IS_PROPERTY_OF_CLASS__EEXPRESSION = "\n" +
		"\t\tlet view : View = self.oclContainer().oclContainer().oclAsType(View) in\n" +
		"\t\t\tself.property -> isEmpty() or (view.class -> notEmpty() and view.class.effectiveProperties -> includes(property))";

	/**
	 * Validates the isPropertyOfClass constraint of '<em>Property Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePropertyElement_isPropertyOfClass(PropertyElement propertyElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.PROPERTY_ELEMENT,
				 propertyElement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "isPropertyOfClass",
				 PROPERTY_ELEMENT__IS_PROPERTY_OF_CLASS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociationElement(AssociationElement associationElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(associationElement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewElement_elementIDUnique(associationElement, diagnostics, context);
		if (result || diagnostics != null) result &= validateAssociationElement_isAssociationOfClass(associationElement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the isAssociationOfClass constraint of '<em>Association Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ASSOCIATION_ELEMENT__IS_ASSOCIATION_OF_CLASS__EEXPRESSION = "\n" +
		"\t\tlet view : View = self.oclContainer().oclContainer().oclAsType(View) in\n" +
		"\t\t\tself.association -> isEmpty() or (self.association.sourceEnd -> notEmpty() and self.association.sourceEnd.type -> notEmpty() and view.class.effectiveTypes -> includes(self.association.sourceEnd.type))";

	/**
	 * Validates the isAssociationOfClass constraint of '<em>Association Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssociationElement_isAssociationOfClass(AssociationElement associationElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.ASSOCIATION_ELEMENT,
				 associationElement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "isAssociationOfClass",
				 ASSOCIATION_ELEMENT__IS_ASSOCIATION_OF_CLASS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewElementGroup(ViewElementGroup viewElementGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(viewElementGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateList(List list, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(list, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(list, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewElement_elementIDUnique(list, diagnostics, context);
		if (result || diagnostics != null) result &= validateAssociationElement_isAssociationOfClass(list, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTable(Table table, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(table, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(table, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(table, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(table, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(table, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(table, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(table, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(table, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(table, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewElement_elementIDUnique(table, diagnostics, context);
		if (result || diagnostics != null) result &= validateAssociationElement_isAssociationOfClass(table, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateText(Text text, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(text, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(text, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(text, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(text, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(text, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(text, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(text, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(text, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(text, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewElement_elementIDUnique(text, diagnostics, context);
		if (result || diagnostics != null) result &= validatePropertyElement_isPropertyOfClass(text, diagnostics, context);
		if (result || diagnostics != null) result &= validateText_textType(text, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the textType constraint of '<em>Text</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TEXT__TEXT_TYPE__EEXPRESSION = "self.property -> isEmpty() or self.property.type -> isEmpty() or self.property.type.oclIsTypeOf(Enumeration) or self.property.type.oclIsTypeOf(DataType)";

	/**
	 * Validates the textType constraint of '<em>Text</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateText_textType(Text text, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.TEXT,
				 text,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "textType",
				 TEXT__TEXT_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDateTimePicker(DateTimePicker dateTimePicker, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(dateTimePicker, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewElement_elementIDUnique(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validatePropertyElement_isPropertyOfClass(dateTimePicker, diagnostics, context);
		if (result || diagnostics != null) result &= validateDateTimePicker_dateType(dateTimePicker, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the dateType constraint of '<em>Date Time Picker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String DATE_TIME_PICKER__DATE_TYPE__EEXPRESSION = "self.property -> isEmpty() or self.property.type -> isEmpty() or (self.property.type.oclIsTypeOf(DataType) and (self.property.type.name = 'Date' or self.property.type.name = 'Time' or self.property.type.name = 'DateTime'))";

	/**
	 * Validates the dateType constraint of '<em>Date Time Picker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDateTimePicker_dateType(DateTimePicker dateTimePicker, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.DATE_TIME_PICKER,
				 dateTimePicker,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "dateType",
				 DATE_TIME_PICKER__DATE_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSelection(Selection selection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(selection, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validateViewElement_elementIDUnique(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validatePropertyElement_isPropertyOfClass(selection, diagnostics, context);
		if (result || diagnostics != null) result &= validateSelection_selectionType(selection, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the selectionType constraint of '<em>Selection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SELECTION__SELECTION_TYPE__EEXPRESSION = "self.property -> isEmpty() or self.property.type -> isEmpty() or self.property.type.oclIsTypeOf(Enumeration) or self.property.type.oclIsTypeOf(DataType)";

	/**
	 * Validates the selectionType constraint of '<em>Selection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSelection_selectionType(Selection selection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.SELECTION,
				 selection,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "selectionType",
				 SELECTION__SELECTION_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSelectionItem(SelectionItem selectionItem, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(selectionItem, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnumerationLiteralItem(EnumerationLiteralItem enumerationLiteralItem, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enumerationLiteralItem, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCondition(Condition condition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(condition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComparisonCondition(ComparisonCondition comparisonCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(comparisonCondition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeCondition(CompositeCondition compositeCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(compositeCondition, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(compositeCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validateCompositeCondition_matchingConditionTypes(compositeCondition, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the matchingConditionTypes constraint of '<em>Composite Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMPOSITE_CONDITION__MATCHING_CONDITION_TYPES__EEXPRESSION = "self.conditions -> forAll(c | c.conditionType = self.conditionType)";

	/**
	 * Validates the matchingConditionTypes constraint of '<em>Composite Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeCondition_matchingConditionTypes(CompositeCondition compositeCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ViewsPackage.Literals.COMPOSITE_CONDITION,
				 compositeCondition,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "matchingConditionTypes",
				 COMPOSITE_CONDITION__MATCHING_CONDITION_TYPES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLink(Link link, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(link, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDomainModelType(DomainModelType domainModelType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(domainModelType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMultiplicity(Multiplicity multiplicity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(multiplicity, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConditionType(ConditionType conditionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComparisonType(ComparisonType comparisonType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeConditionType(CompositeConditionType compositeConditionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateViewLayout(ViewLayout viewLayout, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassOperationType(ClassOperationType classOperationType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ViewsValidator
