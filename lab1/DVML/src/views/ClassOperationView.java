/**
 */
package views;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Operation View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.ClassOperationView#getViewelementgroups <em>Viewelementgroups</em>}</li>
 *   <li>{@link views.ClassOperationView#getOperationType <em>Operation Type</em>}</li>
 *   <li>{@link views.ClassOperationView#getLayout <em>Layout</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getClassOperationView()
 * @model
 * @generated
 */
public interface ClassOperationView extends View {
	/**
	 * Returns the value of the '<em><b>Viewelementgroups</b></em>' containment reference list.
	 * The list contents are of type {@link views.ViewElementGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Viewelementgroups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Viewelementgroups</em>' containment reference list.
	 * @see views.ViewsPackage#getClassOperationView_Viewelementgroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<ViewElementGroup> getViewelementgroups();

	/**
	 * Returns the value of the '<em><b>Operation Type</b></em>' attribute.
	 * The default value is <code>"Read"</code>.
	 * The literals are from the enumeration {@link views.ClassOperationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Type</em>' attribute.
	 * @see views.ClassOperationType
	 * @see #setOperationType(ClassOperationType)
	 * @see views.ViewsPackage#getClassOperationView_OperationType()
	 * @model default="Read" required="true"
	 * @generated
	 */
	ClassOperationType getOperationType();

	/**
	 * Sets the value of the '{@link views.ClassOperationView#getOperationType <em>Operation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation Type</em>' attribute.
	 * @see views.ClassOperationType
	 * @see #getOperationType()
	 * @generated
	 */
	void setOperationType(ClassOperationType value);

	/**
	 * Returns the value of the '<em><b>Layout</b></em>' attribute.
	 * The default value is <code>"Vertical"</code>.
	 * The literals are from the enumeration {@link views.ViewLayout}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout</em>' attribute.
	 * @see views.ViewLayout
	 * @see #setLayout(ViewLayout)
	 * @see views.ViewsPackage#getClassOperationView_Layout()
	 * @model default="Vertical"
	 * @generated
	 */
	ViewLayout getLayout();

	/**
	 * Sets the value of the '{@link views.ClassOperationView#getLayout <em>Layout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout</em>' attribute.
	 * @see views.ViewLayout
	 * @see #getLayout()
	 * @generated
	 */
	void setLayout(ViewLayout value);

} // ClassOperationView
