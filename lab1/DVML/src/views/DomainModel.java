/**
 */
package views;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.DomainModel#getDomainModelTypes <em>Domain Model Types</em>}</li>
 *   <li>{@link views.DomainModel#getAssociations <em>Associations</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getDomainModel()
 * @model
 * @generated
 */
public interface DomainModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Domain Model Types</b></em>' containment reference list.
	 * The list contents are of type {@link views.DomainModelType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Model Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Model Types</em>' containment reference list.
	 * @see views.ViewsPackage#getDomainModel_DomainModelTypes()
	 * @model containment="true"
	 *        extendedMetaData="name='class' namespace=''"
	 * @generated
	 */
	EList<DomainModelType> getDomainModelTypes();

	/**
	 * Returns the value of the '<em><b>Associations</b></em>' containment reference list.
	 * The list contents are of type {@link views.Association}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associations</em>' containment reference list.
	 * @see views.ViewsPackage#getDomainModel_Associations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Association> getAssociations();

} // DomainModel
