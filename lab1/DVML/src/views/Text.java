/**
 */
package views;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Text</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.Text#getFormat <em>Format</em>}</li>
 *   <li>{@link views.Text#isIsLongText <em>Is Long Text</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getText()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='textType'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot textType='self.property -> isEmpty() or self.property.type -> isEmpty() or self.property.type.oclIsTypeOf(Enumeration) or self.property.type.oclIsTypeOf(DataType)'"
 * @generated
 */
public interface Text extends PropertyElement {
	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see #setFormat(String)
	 * @see views.ViewsPackage#getText_Format()
	 * @model
	 * @generated
	 */
	String getFormat();

	/**
	 * Sets the value of the '{@link views.Text#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(String value);

	/**
	 * Returns the value of the '<em><b>Is Long Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Long Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Long Text</em>' attribute.
	 * @see #setIsLongText(boolean)
	 * @see views.ViewsPackage#getText_IsLongText()
	 * @model required="true"
	 * @generated
	 */
	boolean isIsLongText();

	/**
	 * Sets the value of the '{@link views.Text#isIsLongText <em>Is Long Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Long Text</em>' attribute.
	 * @see #isIsLongText()
	 * @generated
	 */
	void setIsLongText(boolean value);

} // Text
