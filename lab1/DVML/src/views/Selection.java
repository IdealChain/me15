/**
 */
package views;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.Selection#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getSelection()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='selectionType'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot selectionType='self.property -> isEmpty() or self.property.type -> isEmpty() or self.property.type.oclIsTypeOf(Enumeration) or self.property.type.oclIsTypeOf(DataType)'"
 * @generated
 */
public interface Selection extends PropertyElement {
	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link views.SelectionItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see views.ViewsPackage#getSelection_Values()
	 * @model containment="true"
	 * @generated
	 */
	EList<SelectionItem> getValues();

} // Selection
