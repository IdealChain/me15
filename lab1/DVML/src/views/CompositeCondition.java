/**
 */
package views;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.CompositeCondition#getCompositeConditionType <em>Composite Condition Type</em>}</li>
 *   <li>{@link views.CompositeCondition#getConditions <em>Conditions</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getCompositeCondition()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='matchingConditionTypes'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot matchingConditionTypes='self.conditions -> forAll(c | c.conditionType = self.conditionType)'"
 * @generated
 */
public interface CompositeCondition extends Condition {
	/**
	 * Returns the value of the '<em><b>Composite Condition Type</b></em>' attribute.
	 * The default value is <code>"AND"</code>.
	 * The literals are from the enumeration {@link views.CompositeConditionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Condition Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Condition Type</em>' attribute.
	 * @see views.CompositeConditionType
	 * @see #setCompositeConditionType(CompositeConditionType)
	 * @see views.ViewsPackage#getCompositeCondition_CompositeConditionType()
	 * @model default="AND" required="true"
	 * @generated
	 */
	CompositeConditionType getCompositeConditionType();

	/**
	 * Sets the value of the '{@link views.CompositeCondition#getCompositeConditionType <em>Composite Condition Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Composite Condition Type</em>' attribute.
	 * @see views.CompositeConditionType
	 * @see #getCompositeConditionType()
	 * @generated
	 */
	void setCompositeConditionType(CompositeConditionType value);

	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' containment reference list.
	 * The list contents are of type {@link views.Condition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditions</em>' containment reference list.
	 * @see views.ViewsPackage#getCompositeCondition_Conditions()
	 * @model containment="true" lower="2" upper="2"
	 * @generated
	 */
	EList<Condition> getConditions();

} // CompositeCondition
