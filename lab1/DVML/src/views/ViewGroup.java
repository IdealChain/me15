/**
 */
package views;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>View Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.ViewGroup#getViews <em>Views</em>}</li>
 *   <li>{@link views.ViewGroup#getName <em>Name</em>}</li>
 *   <li>{@link views.ViewGroup#isWelcomeViewGroup <em>Welcome View Group</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getViewGroup()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validStartView'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot validStartView='self.views -> select(v | v.startView = true) -> size() = 1'"
 * @generated
 */
public interface ViewGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Views</b></em>' containment reference list.
	 * The list contents are of type {@link views.View}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Views</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Views</em>' containment reference list.
	 * @see views.ViewsPackage#getViewGroup_Views()
	 * @model containment="true"
	 * @generated
	 */
	EList<View> getViews();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see views.ViewsPackage#getViewGroup_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link views.ViewGroup#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Welcome View Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Welcome View Group</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Welcome View Group</em>' attribute.
	 * @see #setWelcomeViewGroup(boolean)
	 * @see views.ViewsPackage#getViewGroup_WelcomeViewGroup()
	 * @model required="true"
	 * @generated
	 */
	boolean isWelcomeViewGroup();

	/**
	 * Sets the value of the '{@link views.ViewGroup#isWelcomeViewGroup <em>Welcome View Group</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Welcome View Group</em>' attribute.
	 * @see #isWelcomeViewGroup()
	 * @generated
	 */
	void setWelcomeViewGroup(boolean value);

} // ViewGroup
