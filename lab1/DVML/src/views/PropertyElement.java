/**
 */
package views;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link views.PropertyElement#getProperty <em>Property</em>}</li>
 * </ul>
 *
 * @see views.ViewsPackage#getPropertyElement()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='isPropertyOfClass'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot isPropertyOfClass='\n\t\tlet view : View = self.oclContainer().oclContainer().oclAsType(View) in\n\t\t\tself.property -> isEmpty() or (view.class -> notEmpty() and view.class.effectiveProperties -> includes(property))'"
 * @generated
 */
public interface PropertyElement extends ViewElement {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference.
	 * @see #setProperty(Property)
	 * @see views.ViewsPackage#getPropertyElement_Property()
	 * @model required="true"
	 * @generated
	 */
	Property getProperty();

	/**
	 * Sets the value of the '{@link views.PropertyElement#getProperty <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(Property value);

} // PropertyElement
