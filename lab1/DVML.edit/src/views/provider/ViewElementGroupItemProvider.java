/**
 */
package views.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import views.ViewElementGroup;
import views.ViewsFactory;
import views.ViewsPackage;

/**
 * This is the item provider adapter for a {@link views.ViewElementGroup} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ViewElementGroupItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewElementGroupItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addHeaderPropertyDescriptor(object);
			addViewelementsPropertyDescriptor(object);
			addConditionPropertyDescriptor(object);
			addLayoutPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Header feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHeaderPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ViewElementGroup_header_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ViewElementGroup_header_feature", "_UI_ViewElementGroup_type"),
				 ViewsPackage.Literals.VIEW_ELEMENT_GROUP__HEADER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Viewelements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addViewelementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ViewElementGroup_viewelements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ViewElementGroup_viewelements_feature", "_UI_ViewElementGroup_type"),
				 ViewsPackage.Literals.VIEW_ELEMENT_GROUP__VIEWELEMENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Condition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConditionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ViewElementGroup_condition_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ViewElementGroup_condition_feature", "_UI_ViewElementGroup_type"),
				 ViewsPackage.Literals.VIEW_ELEMENT_GROUP__CONDITION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Layout feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLayoutPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ViewElementGroup_layout_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ViewElementGroup_layout_feature", "_UI_ViewElementGroup_type"),
				 ViewsPackage.Literals.VIEW_ELEMENT_GROUP__LAYOUT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__VIEWELEMENTS);
			childrenFeatures.add(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__CONDITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ViewElementGroup.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ViewElementGroup"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ViewElementGroup)object).getHeader();
		return label == null || label.length() == 0 ?
			getString("_UI_ViewElementGroup_type") :
			getString("_UI_ViewElementGroup_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ViewElementGroup.class)) {
			case ViewsPackage.VIEW_ELEMENT_GROUP__HEADER:
			case ViewsPackage.VIEW_ELEMENT_GROUP__LAYOUT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ViewsPackage.VIEW_ELEMENT_GROUP__VIEWELEMENTS:
			case ViewsPackage.VIEW_ELEMENT_GROUP__CONDITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__VIEWELEMENTS,
				 ViewsFactory.eINSTANCE.createList()));

		newChildDescriptors.add
			(createChildParameter
				(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__VIEWELEMENTS,
				 ViewsFactory.eINSTANCE.createTable()));

		newChildDescriptors.add
			(createChildParameter
				(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__VIEWELEMENTS,
				 ViewsFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__VIEWELEMENTS,
				 ViewsFactory.eINSTANCE.createDateTimePicker()));

		newChildDescriptors.add
			(createChildParameter
				(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__VIEWELEMENTS,
				 ViewsFactory.eINSTANCE.createSelection()));

		newChildDescriptors.add
			(createChildParameter
				(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__CONDITION,
				 ViewsFactory.eINSTANCE.createComparisonCondition()));

		newChildDescriptors.add
			(createChildParameter
				(ViewsPackage.Literals.VIEW_ELEMENT_GROUP__CONDITION,
				 ViewsFactory.eINSTANCE.createCompositeCondition()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ViewsEditPlugin.INSTANCE;
	}

}
