/**
 */
package views.tests;

import junit.framework.TestCase;

import views.DomainModelType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Domain Model Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DomainModelTypeTest extends TestCase {

	/**
	 * The fixture for this Domain Model Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainModelType fixture = null;

	/**
	 * Constructs a new Domain Model Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainModelTypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Domain Model Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DomainModelType fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Domain Model Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainModelType getFixture() {
		return fixture;
	}

} //DomainModelTypeTest
