/**
 */
package views.tests;

import junit.framework.TestCase;

import views.ViewElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>View Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ViewElementTest extends TestCase {

	/**
	 * The fixture for this View Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViewElement fixture = null;

	/**
	 * Constructs a new View Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ViewElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this View Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ViewElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this View Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ViewElement getFixture() {
		return fixture;
	}

} //ViewElementTest
