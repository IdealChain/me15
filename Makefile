# Makefile, Model Engineering WS2015
GROUP=ag_32
IGNORE='*/.*' '*/bin/**'
LABS=$(shell find . -name 'lab?' -type d -exec basename {} \;)
ZIPS=$(LABS:%=$(GROUP)_%.zip)

.PHONY: all clean $(ZIPS)
all: $(ZIPS)

$(ZIPS): $(GROUP)_%.zip: %
	zip -ro $@ $< -x $(IGNORE)

clean:
	$(RM) $(ZIPS)
