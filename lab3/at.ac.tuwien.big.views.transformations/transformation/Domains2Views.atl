-- @path Views=/at.ac.tuwien.big.views.transformations/metamodel/views.ecore

module Domains2Views;
create OUT : Views from IN : Views;

helper def: startViewAlreadySet : Boolean = false;

helper def: id : Integer = 0;
helper context Integer def: increment() : Integer = self + 1;

helper context Views!Class def : getInheritedProperties() : Sequence(Views!Property) =
	if self.superClass.oclIsUndefined()
		then Sequence{}
		else self.superClass.getInheritedProperties()
	endif 
	-> union(self.properties -> select( p | p.oclIsTypeOf(Views!Property))); 

rule DomainModel2ViewModel {
	from
		dm : Views!DomainModel
	to 
		vm : Views!ViewModel (
			viewGroups <- vg
		),
		vg : Views!ViewGroup (
			name <- 'Welcome View Group',
			welcomeViewGroup <- true,
			views <- dm.domainModelElements -> select(c | c.oclIsTypeOf(Views!Class))
		)
}

abstract rule Class2ClassOperationView {
	from
		c : Views!Class
	to 
		cv : Views!ClassOperationView (
			name <- c.name,
			header <- c.name,
			elementGroups <- egProps,
			elementGroups <- Views!Association.allInstances() 
				-> select(a | a.associationEnds -> excluding(a.navigableEnd) -> exists(t | t.type = c))
		),	
		egProps : Views!ElementGroup (
			header <- c.name + ' Information',
			layout <- verticalLayout,
			viewElements <- c.getInheritedProperties() -> collect(prop | thisModule.Property2PropertyElement(prop))
		),
		horizontalLayout : Views!Layout (
			alignment <- #Horizontal
		),
		verticalLayout : Views!Layout (
			alignment <- #Vertical
		)
}

rule Class2CreateView extends Class2ClassOperationView {
	from
		c : Views!Class
	to 
		cv : Views!CreateView (
			layout <- horizontalLayout
		)
	do {
		cv.class <- c;

		c.getInheritedProperties().debug();
		
		if(thisModule.startViewAlreadySet = false) {
			cv.startView <- Views!Association.allInstances() 
				-> collect(p | p.associationEnds).flatten()
				-> collect(t | t.type).flatten() 
				-> excludes(c);
			
			if(cv.startView = true) {
				thisModule.startViewAlreadySet = true;	
			}
		}
		else {
			cv.startView <- false;
		}	
	}
}

lazy abstract rule Property2PropertyElement {
	from
		prop : Views!Property
	to
		e : Views!PropertyElement (
			label <- prop.name.substring(1, 1).toUpper().concat(prop.name.substring(2, prop.name.size())),
			elementID <- thisModule.id.toString()
		)
}

lazy rule Property2TextInteger extends Property2PropertyElement {
	from
		prop : Views!Property (
			prop.type.oclType() = Views!DataType and prop.type.name = 'Integer'	
		)
	to
		e : Views!Text(
			format <- '^[0-9]+$'	
		)
	do {
		e.property <- prop;
		thisModule.id <- thisModule.id.increment();
	}
}

lazy rule Property2TextDouble extends Property2PropertyElement {
	from
		prop : Views!Property (
			prop.type.oclType() = Views!DataType and prop.type.name = 'Double'	
		)
	to
		e : Views!Text(
			format <- '^[0-9]+\\[0-9]$'	
		)
	do {
		e.property <- prop;
		thisModule.id <- thisModule.id.increment();
	}
}

lazy rule Property2TextString extends Property2PropertyElement {
	from
		prop : Views!Property (
			prop.type.oclType() = Views!DataType and prop.type.name = 'String'	
		)
	to
		e : Views!Text(
			format <- '^[a-zA-Z -]+$'
		)
	do {
		e.property <- prop;
		thisModule.id <- thisModule.id.increment();
	}
}

lazy rule Property2DatePicker extends Property2PropertyElement {
	from
		prop : Views!Property (
			prop.type.oclType() = Views!DataType and prop.type.name = 'DateTime'	
		)
	to
		e : Views!DateTimePicker(
			format <- 'dddd, MMMM Do YYYY, h:mm A'
		)
	do {
		e.property <- prop;
		thisModule.id <- thisModule.id.increment();
	}
}

lazy rule Property2Selection extends Property2PropertyElement {
	from
		prop : Views!Property (
			prop.type.oclType() = Views!Enumeration
		)
	to
		e : Views!Selection (
			selectionItems <- prop.type.literals -> collect(l | thisModule.EnumerationLiteral2EnumerationLiteralItem(l))
		)
	do {
		e.property <- prop;
		thisModule.id <- thisModule.id.increment();
	}
}

lazy rule EnumerationLiteral2EnumerationLiteralItem {
	from
		a : Views!EnumerationLiteral
	to 
		eg : Views!EnumerationLiteralItem (
			value <- a.name,
			enumerationLiteral <- a
		)
}

lazy rule Property2List extends Property2PropertyElement {
	from
		prop : Views!Property (
			prop.type.oclType() = Views!Class	
		)
	to
		e : Views!List (
			link <- link
		),
		link : Views!Link (
			targetView <- prop.type,	
			label <- 'Add'
		)
	do {
		thisModule.id <- thisModule.id.increment();
		
		e.association <- Views!Association.allInstances()
				-> select(a | a.navigableEnd = prop).first();
	}
}

rule Association2ElementGroup {
	from
		a : Views!Association
	to 
		eg : Views!ElementGroup (
			header <- a.name.substring(1, 1).toUpper().concat(a.name.substring(2, a.name.size())),
			layout <- verticalLayout,
			viewElements <- thisModule.Property2PropertyElement(a.navigableEnd)
		),
		verticalLayout : Views!Layout (
			alignment <- #Vertical
		)
}