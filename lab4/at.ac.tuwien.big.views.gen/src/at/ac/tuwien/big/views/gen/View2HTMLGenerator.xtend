package at.ac.tuwien.big.views.gen

import at.ac.tuwien.big.views.AssociationElement
import at.ac.tuwien.big.views.Class
import at.ac.tuwien.big.views.ClassIndexView
import at.ac.tuwien.big.views.ClassOperationView
import at.ac.tuwien.big.views.ComparisonCondition
import at.ac.tuwien.big.views.CompositeCondition
import at.ac.tuwien.big.views.ConditionalElement
import at.ac.tuwien.big.views.CreateView
import at.ac.tuwien.big.views.DateTimePicker
import at.ac.tuwien.big.views.DeleteView
import at.ac.tuwien.big.views.EnumerationLiteralItem
import at.ac.tuwien.big.views.LayoutStyle
import at.ac.tuwien.big.views.LinkableElement
import at.ac.tuwien.big.views.List
import at.ac.tuwien.big.views.Property
import at.ac.tuwien.big.views.PropertyElement
import at.ac.tuwien.big.views.ReadView
import at.ac.tuwien.big.views.Selection
import at.ac.tuwien.big.views.SelectionItem
import at.ac.tuwien.big.views.Table
import at.ac.tuwien.big.views.Text
import at.ac.tuwien.big.views.UpdateView
import at.ac.tuwien.big.views.View
import at.ac.tuwien.big.views.ViewGroup
import at.ac.tuwien.big.views.ViewModel
import at.ac.tuwien.big.views.VisibilityCondition
import java.io.File
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator

class View2HTMLGenerator implements IGenerator {

	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		if (resource.contents.get(0) instanceof ViewModel) {
			var viewModel = resource.contents.get(0) as ViewModel
			val welcomegroup_name = viewModel.welcomeGroup.name.name
			val htmlFileName = new File(welcomegroup_name+".html");
			fsa.generateFile(
				htmlFileName.toString,'''
					<!DOCTYPE html>
					<html lang="en" data-ng-app="«welcomegroup_name»App">
					«generateHead(viewModel)»
					<body data-ng-controller="«welcomegroup_name»Controller">

					«generateNavBar(viewModel)»
					«FOR view : viewModel.viewGroups.map[vg | vg.views].flatten»
						«generateView(view, viewModel.welcomeGroup)»
					«ENDFOR»

					</body>
					</html>'''
			)
		}
	}

	def generateHead(ViewModel viewModel) { '''
		<head>
			<title>Views</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
			<script src="../assets/moment-with-locales.js"></script>
			<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
			<script src="../assets/datetimepickerDirective.js"></script>
			<script src="../assets/views.js"></script>
			<script src="«viewModel.welcomeGroup.name.name».js"></script>
			<script type="text/javascript">
					$(document).ready(
						function(){
							view.addWelcomePage('«viewModel.welcomeGroup.welcomePage.name.withoutWhitespace»');
							view.init();
					});
			</script>
		</head>'''
	}

	def generateNavBar(ViewModel model) { '''
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div>
					<ul class="nav navbar-nav">
						«FOR viewGroup : model.viewGroups»
							<li><a href="" class="viewgroup" target="«viewGroup.welcomePage.name.withoutWhitespace»">«viewGroup.name»</a></li>
						«ENDFOR»
					</ul>
				</div>
			</div>
		</nav>
	'''
	}

	def dispatch generateView(ClassIndexView view, ViewGroup welcomeGroup) { '''
		<div class="container" id="«view.name.withoutWhitespace»">
			<h2>«view.header»</h2>
		 	<h3>«view.description»</h3>
			<ul>
				«val classname = view.class_.name.name»
				<li data-ng-repeat="«classname» in «classname»s">
					{{ «classname».«view.class_.id.name.name» }}
					«generateLinks(view)»
				</li>
				«generateAddButtons(view)»
			</ul>
		</div>
	'''
	}

	def dispatch generateView(ReadView view, ViewGroup welcomeGroup) {
		generateReadDeleteView(view)
	}

	def dispatch generateView(DeleteView view, ViewGroup welcomeGroup) {
		generateReadDeleteView(view)
	}

	def dispatch generateView(CreateView view, ViewGroup welcomeGroup) {
		generateCreateUpdateView(view, welcomeGroup)
	}

	def dispatch generateView(UpdateView view, ViewGroup welcomeGroup) {
		generateCreateUpdateView(view, welcomeGroup)
	}

	def generateReadDeleteView(ClassOperationView view) { '''
		<div class="modal fade" id="modal«view.name.withoutWhitespace»">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">«view.header»</h4>
					</div>
					<div class="modal-body">
						<p>«view.description»</p>
						<h5>«view.name»</h5>
						«FOR elem : view.elementGroups.map[eg | eg.viewElements].flatten.filter(PropertyElement)»
							<p>«elem.label»: {{ «view.class_.name.name».«elem.property.name.name» }}</p>
						«ENDFOR»
					</div>
					<div class="modal-footer">
						«generateModalButtons(view)»
					</div>
				</div>
			</div>
		</div>
	'''
	}

	def generateCreateUpdateView(ClassOperationView view, ViewGroup welcomeGroup) { '''
		<div class="container" id="«view.name.withoutWhitespace»">
			<h2>«view.header»</h2>
			<form name="«view.name.withoutWhitespace»Form" novalidate>
				<p>«view.description»</p>
				<div class="panel-group">
					«IF view.layout.alignment == LayoutStyle.HORIZONTAL»
					<div class="row">
					«ENDIF»

					«FOR eg : view.elementGroups»
						<div class="elementgroup«IF view.layout.alignment == LayoutStyle.HORIZONTAL» col-sm-6«ENDIF»" «generateConditionAttribute(eg, view)»>
							<h4>«eg.header»</h4>
							<div class="panel-body">
								«IF eg.layout.alignment == LayoutStyle.HORIZONTAL»
								<div class="form-inline" role="form">
								«ENDIF»

								«FOR ve : eg.viewElements»
									«generateViewElement(ve, view)»
								«ENDFOR»

								«IF eg.layout.alignment == LayoutStyle.HORIZONTAL»
								</div>
								«ENDIF»
							</div>
						</div>
					«ENDFOR»

					«IF view.layout.alignment == LayoutStyle.HORIZONTAL»
					</div>
					«ENDIF»
				</div>
				«generateSaveButtons(view, welcomeGroup)»
			</form>
		</div>
	'''
	}

	def dispatch generateViewElement(PropertyElement pe, View view) { '''
		<div class="form-group">
			«IF pe instanceof Text»
				«pe.generateLabel»
				«pe.generateTextInput(view)»
			«ELSEIF pe instanceof Selection»
				«pe.generateLabel»
				<select data-ng-option class="form-control" id="«pe.elementID»"
					data-ng-model="new«view.class_.name.name».«pe.property.name.name»" «IF pe.property.required»required«ENDIF» «generateConditionAttribute(pe, view)»>
					<option value="" disabled selected>Select your option</option>
					«FOR si : (pe as Selection).selectionItems»
						«si.generateOption»
					«ENDFOR»
				</select>
			«ELSEIF pe instanceof DateTimePicker»
				<div class="row">
					<div class="col-xs-6 col-sm-12">
						<div class="form-group">
							«pe.generateLabel»

							«IF pe.property.type.name == "Date"»
								<div class="input-group date" id="picker«pe.elementID»" style="calendar">
							«ELSEIF pe.property.type.name == "Time"»
								<div class="input-group date" id="picker«pe.elementID»" style="time">
							«ENDIF»

								«pe.generateTextInput(view)»
								<span class="input-group-addon">
									«IF pe.property.type.name == "Date"»
										<span class="glyphicon glyphicon-calendar"></span>
									«ELSEIF pe.property.type.name == "Time"»
										<span class="glyphicon glyphicon-time"></span>
									«ENDIF»
								</span>

							</div>

						</div>
					</div>
				</div>
			«ENDIF»
			«generateErrorSpanTags(pe, view)»
		</div>
	'''
	}

	def generateLabel(PropertyElement pe) { '''
		<label for="«pe.elementID»">«pe.label»«IF pe.property.required»<span>*</span>«ENDIF»:</label>
		'''
	}

	def generateTextInput(PropertyElement pe, View view) { '''
		<«IF pe.textAreaInput»textarea rows="4"«ELSE»input type="text"«ENDIF» class="form-control" id="«pe.elementID»" name="«pe.property.name.name»"
			data-ng-model="new«view.class_.name.name».«pe.property.name.name»" «IF pe.property.required»required«ENDIF» «IF pe.format != null»data-ng-pattern="/«pe.format»/"«ENDIF» «generateConditionAttribute(pe, view)» «IF pe.textAreaInput»>«ELSE»/>«ENDIF»
		«IF pe.textAreaInput»
		</textarea>
		«ENDIF»
		'''
	}

	def dispatch generateOption(SelectionItem si) { '''
		<option value="«si.value»">«si.value»</option>
		'''
	}

	def dispatch generateOption(EnumerationLiteralItem si) { '''
		<option value="«si.enumerationLiteral.value»">«si.enumerationLiteral.name»</option>
		'''
	}

	def generateErrorSpanTags(PropertyElement pe, View view) { '''
		<span class="«view.name.withoutWhitespace»Span" style="color:red"
			data-ng-show="«view.name.withoutWhitespace»Form.«pe.label.name».$dirty && «view.name.withoutWhitespace»Form.«pe.label.name».$invalid">

			«IF pe.property.required»
				<span data-ng-show="«view.name.withoutWhitespace»Form.«pe.label.name».$error.required">Input is mandatory.</span>
			«ENDIF»

			«IF pe.format != null»
				<span data-ng-show="«view.name.withoutWhitespace»Form.«pe.label.name».$error.pattern">Input doesn't match expected pattern.</span>
			«ENDIF»

		</span>
		'''
	}

	def getFormat(PropertyElement pe) {
		switch pe {
			Text case pe.format != null: pe.format
			DateTimePicker case pe.format != null: pe.format
			default: null
		}
	}

	def isTextAreaInput(PropertyElement pe) {
		return pe instanceof Text && (pe as Text).long
	}

	def isRequired(Property property) {
		return property.lowerBound == 1 && property.upperBound == 1
	}

	def dispatch generateViewElement(AssociationElement ae, View view) { '''
		<div class="form-group">
			<div «generateConditionAttribute(ae, view)»>
				<h5>«ae.label»</h5>
				«generateAssociationElements(ae, view)»
				«generateAddButtons(ae)»
			</div>
		</div>
	'''
	}

	def dispatch generateAssociationElements(List l, View view) {
		val class = l.association.navigableEnd.type as Class
		val classname = class.name.name
		'''
		<ul id="«l.elementID»">
			<li data-ng-repeat="«classname» in «classname»s">
				{{ «classname».«class.id.name.name» }}
				«generateLinks(l)»
			</li>
		</ul>
		'''
	}

	def dispatch generateAssociationElements(Table t, View view) {
		val class = t.association.navigableEnd.type as Class
		val classname = class.name.name
		'''
		<table class="table table-striped" id="«t.elementID»">
			<thead>
				<tr>
					«FOR c : t.columns»
						<th>«c.label»</th>
					«ENDFOR»
					«generateLinkHeaders(t)»
				</tr>
			</thead>
			<tbody>
				<tr data-ng-repeat="«classname» in «classname»s">
					«FOR c : t.columns»
						<td> {{ «classname».«c.property.name.name» }} </td>
					«ENDFOR»
					«generateLinks(t)»
				</tr>
			</tbody>
		</table>
		'''
	}

	def generateAddButtons(LinkableElement le) { '''
		«FOR link : le.link.filter[link | link.targetView instanceof CreateView]»
			<button value="«link.targetView.name.withoutWhitespace»" class="btn btn-primary btn-sm">Add</button>
		«ENDFOR»
		'''
	}

	def generateLinks(LinkableElement le) { '''
		«FOR link : le.link.filter[link | !(link.targetView instanceof CreateView)]»
			«IF le instanceof Table»<td>«ENDIF»«generateLink(link.targetView)»«IF le instanceof Table»</td>«ENDIF»
		«ENDFOR»
		'''
	}

	// create the correct number of header columns for the links
	def generateLinkHeaders(Table le) { '''
		«FOR link : le.link.filter[link | !(link.targetView instanceof CreateView)]»
			<th></th>
		«ENDFOR»
		'''
	}

	def dispatch generateLink(ReadView view) {
		val classname = view.class_.name
		'''
		<a href="" data-toggle="modal" data-target="#modal«view.name.withoutWhitespace»"
			data-ng-click="get«classname»(«classname.name».id)">show</a>
		'''
	}

	def dispatch generateLink(DeleteView view) {
		val classname = view.class_.name
		'''
		<a href="" data-toggle="modal" data-target="#modal«view.name.withoutWhitespace»"
			data-ng-click="get«classname»(«classname.name».id)">delete</a>
		'''
	}

	def dispatch generateLink(UpdateView view) {
		val classname = view.class_.name
		'''
		<a href="" data-ng-click="navigation«view.header.withoutWhitespace»('«view.name.withoutWhitespace»'); update«classname»(«classname.name».id)">update</a>
		'''
	}

	def generateSaveButtons(ClassOperationView view, ViewGroup welcomeGroup) {

		// no save button if this is a start view of the welcome group
		val startView = welcomeGroup.views.findFirst[v | v.startView]
		if (view == startView) return ""

		'''
		<button value="«startView.name.withoutWhitespace»" class="btn btn-primary btn-sm"
			data-ng-disabled="«view.name.withoutWhitespace»Form.$invalid"
			data-ng-click="save«view.class_.name»()">
			Save
		</button>
		'''
	}

	def dispatch generateModalButtons(ReadView view) { '''
		<button class="btn btn-default" data-dismiss="modal">Close</button>
		'''
	}

	def dispatch generateModalButtons(DeleteView view) {
		val classname = view.class_.name
		'''
		<button class="btn btn-default" data-dismiss="modal"
			data-ng-click="delete«classname»(«classname.name».id)">Delete</button>
		<button class="btn btn-default" data-dismiss="modal">Cancel</button>
		'''
	}

	def generateConditionAttribute(ConditionalElement ce, View view) {
		if (ce.condition == null) return ""

		val visibility = ce.condition as VisibilityCondition
		val condition = generateCondition(visibility, view)
		switch visibility.type {
			case HIDE:    ''' data-ng-hide="«condition»" '''
			case SHOW:    ''' data-ng-show="«condition»" '''
			case ENABLE:  ''' data-ng-enabled="«condition»" '''
			case DISABLE: ''' data-ng-disabled="«condition»" '''
			default: return ""
		}
	}

	def generateCondition(VisibilityCondition c, View view) {
		switch c {
			CompositeCondition: generateCondition(c, view)
			ComparisonCondition: generateCondition(c, view)
		}
	}

	def generateCondition(CompositeCondition cc, View view) {
		val conditions = cc.composedConditions.map[c | generateCondition(c, view)]

		switch cc.compositionType {
			case AND: conditions.join(" && ")
			case OR:  conditions.join(" || ")
		}
	}

	def generateCondition(ComparisonCondition cc, View view) {
		val property = '''new«view.class_.name.name».«cc.property.label.name»'''
		val value = "'" + cc.comparisonValue + "'"

		switch cc.comparisonType {
			case EQUAL:   '''«property» == «value»'''
			case GREATER: '''«property» > «value»'''
			case LESS:    '''«property» < «value»'''
		}
	}

	def getWelcomeGroup(ViewModel model) {
		return model.viewGroups.findFirst[vg | vg.isWelcomeViewGroup]
	}

	def getWelcomePage(ViewGroup group) {
		return group.views.findFirst[v | v.isStartView]
	}

	def getName(String st){
		return st.toLowerCase.withoutWhitespace
	}

	def getWithoutWhitespace(String st){
		return st.replaceAll("\\W", "")
	}

}
