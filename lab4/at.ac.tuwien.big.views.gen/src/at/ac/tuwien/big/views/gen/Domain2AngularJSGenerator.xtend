package at.ac.tuwien.big.views.gen

import java.io.File
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.ac.tuwien.big.views.DomainModel
import at.ac.tuwien.big.views.Class

class Domain2AngularJSGenerator implements IGenerator {
	
	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		if (resource.contents.get(0) instanceof DomainModel) {
			var domainModel = resource.contents.get(0) as DomainModel
			val angularJSFileName = new File(getFirstClassName(domainModel).toLowerCase+".js");
			fsa.generateFile(
				angularJSFileName.toString,
				'''
				«generateModule(domainModel)»
				«generateService(domainModel)»
				«generateController(domainModel)»'''
			)
		}
	}
	
	def generateModule(DomainModel model) {
		'''var module = angular.module('«getFirstClassName(model)»App', []);'''
	}
	
	def generateService(DomainModel model) { '''
		module.service('«getFirstClassName(model)»Service', function () {
			«FOR cl : model.domainModelElements.filter(Class)»
				«cl.generateService»
			«ENDFOR»
		});'''
	}
	
	def generateService(Class cl) { '''
		var «cl.name.toLowerCase»s = [];
		var «cl.name.toLowerCase»id = 0;
		
		this.save«cl.name»Service = function («cl.name.toLowerCase») {
			if («cl.name.toLowerCase».id == null) {
				«cl.name.toLowerCase».id = «cl.name.toLowerCase»id++;
				«cl.name.toLowerCase»s.push(«cl.name.toLowerCase»);
			} else {
				for (i in «cl.name.toLowerCase»s) {
					if («cl.name.toLowerCase»s[i].id == «cl.name.toLowerCase».id) {
						«cl.name.toLowerCase»s[i] = «cl.name.toLowerCase»;
					}
				}
			}
		}
		
		this.get«cl.name»Service = function (id) {
			for (i in «cl.name.toLowerCase»s) {
				if («cl.name.toLowerCase»s[i].id == id) {
					return «cl.name.toLowerCase»s[i];
				}   
			}
		}
		
		this.delete«cl.name»Service = function (id) {
			for (i in «cl.name.toLowerCase»s) {
				if («cl.name.toLowerCase»s[i].id == id) {
					«cl.name.toLowerCase»s.splice(i, 1);
				}
			}
		}
		
		this.list«cl.name»Service = function () {
			return «cl.name.toLowerCase»s;
		}
		
		'''
	}
	
	def generateController(DomainModel model) {	'''
		module.controller('«getFirstClassName(model)»Controller', function ($scope, «getFirstClassName(model)»Service) {
			«FOR cl : model.domainModelElements.filter(Class)»
				«cl.generateController(model)»
			«ENDFOR»
		});'''
	}
	
	def generateController(Class cl, DomainModel model) { '''
		$scope.«cl.name.toLowerCase»s = «getFirstClassName(model)»Service.list«cl.name»Service();
		
		$scope.save«cl.name» = function () {
			instituteService.save«cl.name»Service($scope.new«cl.name.toLowerCase»);
			$scope.new«cl.name.toLowerCase» = {};
		}

		$scope.delete«cl.name» = function (id) {
			instituteService.delete«cl.name»Service(id);
		}

		$scope.update«cl.name» = function (id) {
			$scope.new«cl.name.toLowerCase» = angular.copy(«getFirstClassName(model)»Service.get«cl.name»Service(id));
		}

		$scope.get«cl.name» = function (id) {
			$scope.«cl.name.toLowerCase» = angular.copy(«getFirstClassName(model)»Service.get«cl.name»Service(id));
		}

		$scope.navigation«cl.name» = function (targetView) {
			$(".container").hide();
			var view = angular.element('#'+targetView);
			view.show();
		}
		
		'''
	}
	
	def getFirstClassName(DomainModel model) {
		var classname = model.domainModelElements.filter(Class).get(0)
		return classname.name.toLowerCase.replaceAll("\\W", "");
	}	
}